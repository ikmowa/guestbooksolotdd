package com.lorenz_niels.guestbook.services;

import com.lorenz_niels.guestbook.repositories.GuestBookRepostiory;
import com.lorenz_niels.guestbook.services.exceptions.GuestBookServiceException;
import org.springframework.stereotype.Service;
import com.lorenz_niels.guestbook.model.GuestBook;

import java.util.*;

@Service
public class GuestBookRepoService implements GuestBookService {

    private final GuestBookRepostiory repository;

    public GuestBookRepoService(final GuestBookRepostiory repository) {
        this.repository = repository;
    }

    public Optional<GuestBook> getGuestBookByUUID(final UUID uuid) {
        return repository.findById(uuid);
    }

    public List<GuestBook> getAllGuestBooks() {
        return repository.findAll();
    }

    public GuestBook createNewGuestBook(GuestBook guestBook) {
        return repository.save(guestBook);
    }

    @Override
    public GuestBook updateGuestBook(GuestBook guestBook) {
        Optional<GuestBook> maybeBook = repository.findById(guestBook.getId());
        if (maybeBook.isPresent()) {
            return repository.save(maybeBook.get());
        } else {
            throw new GuestBookServiceException("The guestbook you tried to update does not exist.");
        }
    }

    @Override
    public void removeGuestBook(UUID id) {
        Optional<GuestBook> maybeBook = repository.findById(id);
        if (maybeBook.isPresent()) {
            repository.delete(maybeBook.get());
        } else {
            throw new GuestBookServiceException("The guestbook you tried to delete does not exist");
        }
    }
}
