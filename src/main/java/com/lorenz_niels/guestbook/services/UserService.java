package com.lorenz_niels.guestbook.services;

import com.lorenz_niels.guestbook.model.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService {
    Optional<User> findUserById(UUID id);

    List<User> findAllUsers();

    User save(User user);

    User update(User user);

    void delete(UUID id);

    User findByName(String name);

    Boolean makeNewUser(User user);

    public String getCurrentUserName();

    public User getCurrentUserObjectByName(String username);

    public User getCurrentUserObject();
}
