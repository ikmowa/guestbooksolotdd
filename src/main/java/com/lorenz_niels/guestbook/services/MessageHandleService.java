package com.lorenz_niels.guestbook.services;

import com.lorenz_niels.guestbook.model.Messages;
import com.lorenz_niels.guestbook.model.User;
import com.lorenz_niels.guestbook.repositories.MessageRepostiory;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.UUID;

@Service
public class MessageHandleService implements MessageService{
    private final MessageRepostiory repository;
    private final GuestBookService guestBookService;
    private final UserService userService;

    public MessageHandleService(final MessageRepostiory repository, final GuestBookService service, UserService userService) {
        this.repository = repository;
        this.guestBookService = service;
        this.userService = userService;
    }


    public Messages createNewMessage(Messages messages, UUID uuid) {
        User user = userService.getCurrentUserObject();
        if (user==null) {throw new NoSuchElementException("Auth user is not found.");}
        messages.setUser(user);
        messages.setGuestbook(guestBookService.getGuestBookByUUID(uuid).orElseThrow());//TODO excp
        return repository.save(messages);
    }

    public boolean deleteMessage(UUID uuid) {
        repository.deleteById(uuid);
        return true;
    }
}
