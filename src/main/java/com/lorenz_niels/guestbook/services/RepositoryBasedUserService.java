package com.lorenz_niels.guestbook.services;

import com.lorenz_niels.guestbook.model.User;
import com.lorenz_niels.guestbook.repositories.UserRepository;
import com.lorenz_niels.guestbook.services.exceptions.UserServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RepositoryBasedUserService implements UserService {
    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationFacade authenticationFacade;


    public RepositoryBasedUserService(final UserRepository repository, final PasswordEncoder passwordEncoder, final AuthenticationFacade authenticationFacade) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
        this.authenticationFacade = authenticationFacade;
    }

    @Override
    public Optional<User> findUserById(final UUID id) {
        return repository.findById(id);
    }

    @Override
    public List<User> findAllUsers() {
        return repository.findAll();
    }

    @Override
    public User save(final User user) {
        return repository.save(user);
    }

    @Override
    public User update(final User user) {
        Optional<User> maybeUser = repository.findById(user.getId());
        if (maybeUser.isPresent()) {
            return repository.save(maybeUser.get());
        } else {
            throw new UserServiceException("The user you tried to update does not exist.");
        }
    }

    @Override
    public void delete(UUID id) {
        Optional<User> maybeUser = repository.findById(id);
        if (maybeUser.isPresent()) {
            repository.delete(maybeUser.get());
        } else {
            throw new UserServiceException("The user you tried to delete does not exist");
        }
    }

    @Override
    public User findByName(String name) {
        return repository.findByUsername(name).orElseThrow();//TODO throw something
    }

    private String[] strings = {"login", "logout", "index", "register",
            "detail", "list", "user", "guestbook", "403", "404"};
    private List<String> reservedWords = new ArrayList<>();

    @Override
    public Boolean makeNewUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        reservedWords.addAll(Arrays.asList(strings));
        boolean userCreated = false;

        try {
            // Test of username nog vrij is
            Optional<User> usernameFreeTest = repository.findByUsername(user.getUsername());
            if (usernameFreeTest.isPresent() || reservedWords.stream().anyMatch(n -> n.equals(user.getUsername()))) {
            } else {
                repository.save(user); // Nieuwe user wegschrijven na username availability check en email validation
                userCreated = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userCreated;
    }

    public String getCurrentUserName() {
        Authentication authentication = authenticationFacade.getAuthentication();
        return authentication==null?null:authentication.getName();
    }

    public User getCurrentUserObjectByName(String username) {
        return repository.findByUsername(username).orElse(null);
    }

    public User getCurrentUserObject() {
        String username = this.getCurrentUserName();
        return username==null?null:this.getCurrentUserObjectByName(username);//TODO throw notfound
    }
}
