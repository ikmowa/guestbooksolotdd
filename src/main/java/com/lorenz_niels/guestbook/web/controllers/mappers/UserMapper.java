package com.lorenz_niels.guestbook.web.controllers.mappers;

import com.lorenz_niels.guestbook.model.User;
import com.lorenz_niels.guestbook.web.model.UserDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(uses = UUIDMapper.class)
public interface UserMapper {



    UserDTO mapUserEntityToUserDTO (User user);
    User mapUserDTOToUserEntity (UserDTO userDTO);
    List<UserDTO> mapUsersToDTOs (List<User> users);

}
