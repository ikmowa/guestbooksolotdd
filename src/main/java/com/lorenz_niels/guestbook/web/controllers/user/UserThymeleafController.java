package com.lorenz_niels.guestbook.web.controllers.user;

import com.lorenz_niels.guestbook.model.User;
import com.lorenz_niels.guestbook.services.GuestBookService;
import com.lorenz_niels.guestbook.services.UserService;
import com.lorenz_niels.guestbook.web.controllers.mappers.UserMapper;
import com.lorenz_niels.guestbook.web.model.UserDTO;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Controller
@RequestMapping("/user")
public class UserThymeleafController {

    private final UserMapper userMapper;
    private final UserService userService;

    public UserThymeleafController(final UserMapper userMapper, final UserService userService) {
        this.userMapper = userMapper;
        this.userService = userService;
    }

    @GetMapping(value = "/", produces = MediaType.TEXT_HTML_VALUE)
    public String presentUserList(Model model) {
        model.addAttribute("users", userMapper.mapUsersToDTOs(userService.findAllUsers()));
        return "user/list";
    }

    @PostMapping(value = "/", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.TEXT_HTML_VALUE)
    public String postUser(@ModelAttribute UserDTO userDTO, Model model) {
        if (userDTO.getId() == null) {
            User userSaved = userService.save(userMapper.mapUserDTOToUserEntity(userDTO));
            userMapper.mapUserEntityToUserDTO(userSaved);
        } else {
            User userUpdated = userService.update(userMapper.mapUserDTOToUserEntity(userDTO));
            userMapper.mapUserEntityToUserDTO(userUpdated);
        }
        return "redirect:/user/" + userDTO.getId();

    }

    @GetMapping(value = "/{id}", produces = MediaType.TEXT_HTML_VALUE)
    public String presentUserDetail(Model model, @PathVariable("id") final UUID id) {
        model.addAttribute("user", userService
                .findUserById(id)
                .map(userMapper::mapUserEntityToUserDTO)
                .orElse(null))/*.addAttribute("userGuestBooks", guestBookService.getGuestBooksByUserId(id))*/;
        return "user/detail";
    }


}
