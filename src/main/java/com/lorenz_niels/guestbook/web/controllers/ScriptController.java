package com.lorenz_niels.guestbook.web.controllers;

import com.lorenz_niels.guestbook.DatabaseGeneratorComponent;
import com.lorenz_niels.guestbook.web.controllers.guestbook.GuestBookDelegateController;
import com.lorenz_niels.guestbook.web.model.GuestBookDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/script")
public class ScriptController {
    @Autowired
    GuestBookDelegateController delegate;

    @Autowired
    DatabaseGeneratorComponent script;

    @GetMapping
    public List<GuestBookDTO> handleGet() {
        script.run();
        return delegate.handleGETAll();
    }
}
