package com.lorenz_niels.guestbook.web.controllers.guestbook;

import com.github.rjeschke.txtmark.Processor;
import com.lorenz_niels.guestbook.services.UserService;
import com.lorenz_niels.guestbook.web.model.GuestBookDTO;
import com.lorenz_niels.guestbook.web.model.MessagesDTO;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Controller
@RequestMapping("/guestbook")
public class GuestBookThymeLeafController {

    private final GuestBookDelegateController delegate;
    private final UserService service;

    public GuestBookThymeLeafController(final GuestBookDelegateController delegate, UserService service) {
        this.delegate = delegate;
        this.service = service;
    }

    @ModelAttribute("role")
    public String getRole() {
        return service.getCurrentUserObject()==null?null:service.getCurrentUserObject().getRole();
    }


    @GetMapping(value = "/", produces = MediaType.TEXT_HTML_VALUE)
    public String handleGetAll(Model model) {
        model.addAttribute("guestBooks", delegate.handleGETAll());
        model.addAttribute("guestbookNew", new GuestBookDTO());
        return "guestbook/list";
    }

    @GetMapping(value = "/{id}", produces = {MediaType.TEXT_HTML_VALUE})
    public String handleGet(Model model, @PathVariable("id") final UUID uuid) {
        GuestBookDTO guestBook = delegate.handleGET(uuid);
        model.addAttribute("guestBook", guestBook);
        model.addAttribute("messagesList", proccesMD(guestBook.getMessages()));
        model.addAttribute("messageNew", new MessagesDTO());
        return "guestbook/detail";
    }

    private List<MessagesDTO> proccesMD(Set<MessagesDTO> set) {
        List<MessagesDTO> list = new ArrayList<>(set);
                list.forEach(m -> m.setComment(Processor.process(m.getComment())));
                list.sort(MessagesDTO::compareTo);
        return list;

    }

    @PostMapping(value = "/" , produces = MediaType.TEXT_HTML_VALUE)
    @Secured("ROLE_ADMIN")
    public String handlePost(@ModelAttribute GuestBookDTO guestBookDTO) {
        delegate.handlePOST(guestBookDTO);
        return "redirect:/guestbook/";
    }

    @DeleteMapping("/del/{id}")
    @Secured("ROLE_ADMIN")
    public String handleDelete(@PathVariable("id") UUID uuid) {
        delegate.handleDELETE(uuid);
        return "redirect:/guestbook/";
    }


}
