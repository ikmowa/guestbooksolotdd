package com.lorenz_niels.guestbook.web.controllers.user;

import com.lorenz_niels.guestbook.model.User;
import com.lorenz_niels.guestbook.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/register")
public class RegisterController {

    private UserService userService;

    public RegisterController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String forward() {
        return "register";
    }

    @PostMapping
    public String postNewUser(User user) {
        if (userService.makeNewUser(user)) {
            return "redirect:login";
        } else {
            return "redirect:register";
        }
    }
}

