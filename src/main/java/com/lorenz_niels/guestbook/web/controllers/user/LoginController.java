package com.lorenz_niels.guestbook.web.controllers.user;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginController {

    @GetMapping
    public String forward() {
        return "login";
    }

    @PostMapping
    public String loginUser() {
        return "redirect:/guestbook";
    }
}

