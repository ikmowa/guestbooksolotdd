package com.lorenz_niels.guestbook.web.controllers.guestbook;

import com.lorenz_niels.guestbook.services.GuestBookRepoService;
import com.lorenz_niels.guestbook.web.controllers.mappers.GuestBookMapper;
import com.lorenz_niels.guestbook.web.model.GuestBookDTO;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.UUID;

@Component
public class GuestBookDelegateController {

    private final GuestBookRepoService service;
    private final GuestBookMapper mapper;

    public GuestBookDelegateController(final GuestBookRepoService service, final GuestBookMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    public GuestBookDTO handleGET(@PathVariable("id") final UUID uuid) {
        return mapper.mapGuestBookEntityToGuestBookDTO(
                service.getGuestBookByUUID(uuid)
                        .orElseThrow(IllegalArgumentException::new));
    }

    public List<GuestBookDTO> handleGETAll() {
        return mapper.mapGuestBookEntityToGuestBookDTO(
                service.getAllGuestBooks());
    }

    public GuestBookDTO handlePOST(@RequestBody final GuestBookDTO guestBookDTO) {
        return mapper.mapGuestBookEntityToGuestBookDTO(
                service.createNewGuestBook(
                        mapper.mapGuestBookDTOtoGuestBookEntity(guestBookDTO)));
    }

    public GuestBookDTO handlePUT(@RequestBody final GuestBookDTO guestBookDTO) {
        return mapper.mapGuestBookEntityToGuestBookDTO(
                service.updateGuestBook(
                        mapper.mapGuestBookDTOtoGuestBookEntity(guestBookDTO)));
    }

    public void handleDELETE(@PathVariable("id") final UUID id) {
        service.removeGuestBook(id);
    }
}
