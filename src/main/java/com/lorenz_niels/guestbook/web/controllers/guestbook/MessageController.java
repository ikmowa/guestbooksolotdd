package com.lorenz_niels.guestbook.web.controllers.guestbook;

import com.lorenz_niels.guestbook.services.MessageService;
import com.lorenz_niels.guestbook.web.controllers.mappers.MessageMapper;
import com.lorenz_niels.guestbook.web.model.MessagesDTO;
import org.owasp.html.Sanitizers;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Controller
@RequestMapping("guestbook/{guestBookId}")
public class MessageController {

    private final MessageMapper mapper;
    private final MessageService service;

    public MessageController(final MessageMapper mapper, final MessageService service) {
        this.mapper = mapper;
        this.service = service;
    }

    @PostMapping("/new/message")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public String handlePost(@ModelAttribute MessagesDTO messages, @PathVariable("guestBookId") UUID uuid) {
        messages.setComment(Sanitizers.LINKS.sanitize(messages.getComment()));

        service.createNewMessage(mapper.mapMessagesDTOtoMessagesEntity(messages), uuid);
        return "redirect:/guestbook/" + uuid.toString();
    }

    private String addPreTags(String s) {
        StringBuilder builder = new StringBuilder(s);
        builder.insert(0,"<pre>");
        builder.append("</pre>");
        return builder.toString();
    }

    @DeleteMapping("/del/message/{id}")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public String handleDelete(@PathVariable("id") UUID uuidMessage, @PathVariable("guestBookId") UUID uuidGuestbook) {
        service.deleteMessage(uuidMessage);
        return "redirect:/guestbook/" + uuidGuestbook.toString();
    }
}
