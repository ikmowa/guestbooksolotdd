package com.lorenz_niels.guestbook.web.controllers.guestbook;

import com.lorenz_niels.guestbook.DatabaseGeneratorComponent;
import com.lorenz_niels.guestbook.model.GuestBook;
import com.lorenz_niels.guestbook.web.model.GuestBookDTO;
import com.lorenz_niels.guestbook.web.model.validation.PostValidation;
import com.lorenz_niels.guestbook.web.model.validation.PutValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/guestbook")
public class GuestBookRestController {

    @Autowired
    DatabaseGeneratorComponent dbscript;

    private final GuestBookDelegateController delegate;

    public GuestBookRestController(final GuestBookDelegateController delegate) {
        this.delegate = delegate;
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuestBookDTO> handleGET(@PathVariable("id") final UUID uuid) {
        return new ResponseEntity<>(delegate.handleGET(uuid), HttpStatus.OK);
    }

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<GuestBookDTO> handleGETAll() {
        dbscript.run();
        return delegate.handleGETAll();
    }

    @PostMapping(value = "/", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GuestBookDTO> handlePOST(@RequestBody @Validated(PostValidation.class) final GuestBookDTO guestBookDTO) {
        return new ResponseEntity<>(delegate.handlePOST(guestBookDTO), HttpStatus.CREATED);
    }

    @PutMapping(value = "/")
    public ResponseEntity<GuestBookDTO> handlePUT(@RequestBody @Validated(PutValidation.class) final GuestBookDTO guestBookDTO) {
        return new ResponseEntity<>(delegate.handlePUT(guestBookDTO), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<GuestBookDTO> handleDELETE(@PathVariable("id") final UUID id) {
        delegate.handleDELETE(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
