package com.lorenz_niels.guestbook.web.model;

import javax.validation.constraints.NotBlank;
import java.util.Set;

public class GuestBookDTO extends AbstractGuestbookDTO {

    @NotBlank
    private String name;

    private Set<MessagesDTO> messages;

    public Set<MessagesDTO> getMessages() {
        return messages;
    }

    public void setMessages(Set<MessagesDTO> messages) {
        this.messages = messages;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
