package com.lorenz_niels.guestbook.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class GuestBook extends AbstractGuestbookEntity {

    @Column(nullable = false)
    private String name;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, mappedBy = "guestbook")
    private Set<Messages> messages;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Messages> getMessages() {
        return messages;
    }

    public void setMessages(Set<Messages> messages) {
        this.messages = messages;
    }
}
