package com.lorenz_niels.guestbook.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import java.time.LocalTime;

@Entity
public class Messages extends AbstractGuestbookEntity {

    @Column(nullable = false)
    private String title;

    @Lob
    @Column(length=5120)
    private String comment;

    @ManyToOne(optional = false)
    private User user;

    @ManyToOne(optional = false)
    private GuestBook guestbook;

    private LocalTime date = LocalTime.now();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public GuestBook getGuestbook() {
        return guestbook;
    }

    public void setGuestbook(GuestBook guestbook) {
        this.guestbook = guestbook;
    }

    public LocalTime getDate() {
        return date;
    }

    public void setDate(LocalTime date) {
        this.date = date==null?LocalTime.now():date;
    }


}
