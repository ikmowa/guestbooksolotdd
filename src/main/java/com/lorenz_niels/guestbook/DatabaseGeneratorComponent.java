package com.lorenz_niels.guestbook;

import com.lorenz_niels.guestbook.model.GuestBook;
import com.lorenz_niels.guestbook.model.Messages;
import com.lorenz_niels.guestbook.model.User;
import com.lorenz_niels.guestbook.repositories.GuestBookRepostiory;
import com.lorenz_niels.guestbook.repositories.MessageRepostiory;
import com.lorenz_niels.guestbook.repositories.UserRepository;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class DatabaseGeneratorComponent {

    GuestBookRepostiory guestBookRepostiory;
    MessageRepostiory messageRepostiory;
    UserRepository userRepostiory;

    public DatabaseGeneratorComponent(GuestBookRepostiory guestBookRepostiory, MessageRepostiory messageRepostiory, UserRepository userRepostiory) {
        this.guestBookRepostiory = guestBookRepostiory;
        this.messageRepostiory = messageRepostiory;
        this.userRepostiory = userRepostiory;
    }

    String[] randomWords = {"disease","states","pass","win","truncheon",
            "published","force","report","entire","open","directed",
            "went","view","sure","strait","news","corslet","hock",
            "fan","proposed","operator","tussock","account","difference",
            "honor","knowledge","calculate","accept","while","faculty",
            "confuse","families","pale","hauberk","ask","realize",
            "threaten","other","year","that's","march","unless",
            "foreswear","hit","cut","reft","chief","objection",
            "choose","advise","membership","entirely","silence",
            "clamant","almost","victory","fall","sample","seeing",
            "shun","gun","agency","ell","rich","garden","factors",
            "doing","translate","changed","pulled","box","dregs",
            "act","adventure","addition","strand","fast","fortnight",
            "governor","wildered","forms","gorcrow","returned","wage",
            "sooth","","allow","groups","suggest","moment","like",
            "stoop","group","interested","special","bear","sprent","security",
            "family","I'd"};

    Random random = new Random();

    public void run() {

        for (int i = 0; i < 15; i++) {
            User user = new User();
            user.setUsername(randomWords[random.nextInt(randomWords.length - 1)]);
            user.setPassword("root");
            user.setRole("user");
            user.setMessages(new HashSet<>());
            userRepostiory.save(user);
        }

        for (int i = 0; i < 5; i++) {
            GuestBook guestBook = new GuestBook();
            guestBook.setMessages(new HashSet<>());
            guestBook.setName("guestbook of " + randomWords[random.nextInt(randomWords.length - 1)]);
            guestBookRepostiory.save(guestBook);
        }

        List<User> userIds = userRepostiory.findAll();
        List<GuestBook> guestIds = guestBookRepostiory.findAll();

            int messageCount = 150;
            for (int y = 0; y < messageCount; y++) {
                User user = userIds.get(random.nextInt(userIds.size()));
                GuestBook guestBook = guestIds.get(random.nextInt(guestIds.size()));
                Messages messages = new Messages();

                messages.setTitle(randomWords[random.nextInt(randomWords.length - 1)] +
                        randomWords[random.nextInt(randomWords.length - 1)]);

                StringBuilder stringBuilder = new StringBuilder();

                int wordCount = random.nextInt(10) + 1;
                for (int j = 0; j < wordCount; j++) {
                    stringBuilder.append(randomWords[random.nextInt(randomWords.length - 1)] +
                            randomWords[random.nextInt(randomWords.length - 1)] + " ");

                }

                messages.setComment(stringBuilder.toString());

                messages.setUser(user);
                messages.setGuestbook(guestBook);

                guestBook.getMessages().add(messages);
                user.getMessages().add(messages);

                userRepostiory.save(user);
                guestBookRepostiory.save(guestBook);
                messageRepostiory.save(messages);

            }
        }
    }

